﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace datatype
{
    class Program
    {
        static void Main(string[] args)
        {
            //data type 
            // I. integers
            int Add_Balance = 200;
            int TDD_d = 30;

            uint u_II = 20;
            uint u_I = 30;

            Console.WriteLine("Hello Test");
            Console.WriteLine(Add_Balance);
            Console.WriteLine(TDD_d);

            Console.WriteLine("Max value of int is {0}", int.MaxValue);
            Console.WriteLine("Max value of int is {0}", int.MinValue);

            Console.WriteLine("Max value of uint is {0}", uint.MaxValue);
            Console.WriteLine("Max value of uint is {0}", uint.MinValue);

            //II. Fractions
            float float_number = 2.56f;
            double double_num = 16.22562;

            Console.WriteLine(float_number);
            Console.WriteLine(double_num);

            Console.WriteLine("Max value of float is {0}", float.MaxValue);
            Console.WriteLine("Max value of float is {0}", float.MinValue);
            Console.WriteLine("Max value of float is {0}", float.NaN);

            Console.WriteLine("Max value of double is {0}", double.MinValue);
            Console.WriteLine("Max value of double is {0}", double.MaxValue);

            Console.WriteLine("Max value of decimal is {0}", decimal.MinValue);
            Console.WriteLine("Max value of decimal is {0}", decimal.MaxValue);

            // III. Logical 
            bool logical_true = true;
            bool logical_false = false;

            Console.WriteLine(logical_true);
            Console.WriteLine(logical_false);

            //IV. Texts
            char test_char = 'c';
            char one = '1';
            string two = "22 lorem ipsome";

            Console.WriteLine(two);


            Console.ReadLine();

        }
    }
}
